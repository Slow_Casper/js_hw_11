const icon = document.querySelectorAll(".icon-password");
const btn = document.querySelector(".btn");

function showPass (event){
    const target = event.target;
    const input = target.previousElementSibling;
    if(input.getAttribute("type") === "password"){
        input.setAttribute("type", "text");
        if(target.classList.contains("fa-eye")){
            target.classList.replace("fa-eye", "fa-eye-slash")
        }
    }else{
        input.setAttribute("type", "password");
        if(target.classList.contains("fa-eye-slash")){
            target.classList.replace("fa-eye-slash", "fa-eye")
        }
    }
};

icon.forEach((element) => {
    element.addEventListener("click", showPass)
});

btn.addEventListener("click", function(event){
    event.preventDefault();
    const paragr = document.querySelector(".pass_description");
    paragr.style.display = "none";
    const pass = document.querySelector(".pass");
    const confirm = document.querySelector(".confirm_pass");

    if (pass.value === confirm.value) {
        alert("Ласкаво просимо!");
    }else{
        paragr.style.display = "block";
    }
    
});
